import React, { Component } from "react";
import { View, Text, Image, TextInput, TouchableOpacity } from "react-native";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    const { viewStyle, inputStyle } = styles;
    return (
      <View style={viewStyle}>
        <TouchableOpacity onPress={this.props.onSettingsPress}>
          <Image
            source={require("./icons/settings.png")}
            style={{ height: 30, width: 30, marginRight: 5 }}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.props.onHistoryPress}>
          <Image
            source={require("./icons/history.png")}
            style={{ height: 30, width: 30, marginRight: 10 }}
          />
        </TouchableOpacity>
        <TextInput
          style={inputStyle}
          value={this.props.searchPlace}
          placeholder={"Search location"}
          onChangeText={this.props.onChangeText}
        />
        <TouchableOpacity onPress={this.props.onSearchPress}>
          <Image
            source={require("./icons/search.png")}
            style={{ height: 30, width: 30, marginRight: 20 }}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  textStyle: {
    fontSize: 30,
    fontWeight: "bold",
    color: "white"
  },
  viewStyle: {
    backgroundColor: "#ecebe7",
    display: "flex",
    flexDirection: "row",
    padding: 5,
    alignItems: "center",
    justifyContent: "center",
    elevation: 5,
    position: "relative",
    borderButtomWidth: 2
  },
  inputStyle: {
    color: "#000",
    flex: 2,
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18
  }
};

export default Header;
