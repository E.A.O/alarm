import React, { Component } from "react";
import { View, TextInput } from "react-native";
import { Header, Icon } from "react-native-elements";

class TopMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Header backgroundColor="#4f5858">
          <Icon
            name="menu"
            color="#c2c2c2"
            size={50}
            containerStyle={{ height: 40 }}
            onPress={this.props.onMenuPress}
          />
          <TextInput
            placeholder="Search location"
            style={{
              backgroundColor: "#fff",
              borderRadius: 15,
              height: 40,
              marginLeft: 50,
              fontSize: 18,
              paddingRight: 5,
              paddingLeft: 15,
              width: 200
            }}
            value={this.props.value}
            onChangeText={this.props.onChangeText}
          />
          <Icon
            name="search"
            color="#c2c2c2"
            size={40}
            containerStyle={{ height: 40 }}
            onPress={this.props.onSearchPress}
          />
          <Icon
            name="settings"
            color="#c2c2c2"
            size={40}
            containerStyle={{ height: 40 }}
            onPress={() => alert(this.props.value)}
          />
        </Header>
      </View>
    );
  }
}

export default TopMenu;
