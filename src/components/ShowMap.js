import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  Alert,
  TextInput,
  AppState
} from "react-native";
import MapView, { Marker, Circle } from "react-native-maps";
import Communications from "react-native-communications";
import Geocoder from "react-native-geocoder-reborn";
import geolib from "geolib";
import PushNotification from "react-native-push-notification";
import MarkerModal from "./Modals/MarkerModal";
import History from "./Modals/History";
import Settings from "./Modals/Settings";

export default class ShowMap extends Component {
  constructor(props) {
    super(props);
    this.map = null;
    navigator.geolocation.watchPosition(position =>
      this.setState(
        {
          region: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421
          },
          latlng: {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
          }
        },
        this.compare
      )
    );
    this.state = {
      email: "",
      phone: "",
      message: "",
      task: "",
      radius: 200,
      latitude: 0,
      longitude: 0,
      title: "",
      description: "",
      alarm: false,
      visible: false,
      history: true,
      coordinate: {},
      latlng: {},
      region: {
        latitude: 6.672645,
        longitude: -1.563173,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421
      },
      markers: [],
      marker: {}
    };
  }

  componentDidMount() {
    AppState.addEventListener("change", this.call);
    navigator.geolocation.getCurrentPosition(position => {
      this.setState({
        region: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: 0.922,
          longitudeDelta: 0.0421
        }
      });
    });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.search === true) {
      this.findLocation();
    }
  }

  componentWillUnmount() {
    AppState.removeEventListener("change", this.call);
  }

  componentDidUpdate(allProps, allState) {
    if (allState.alarm === true) {
      this.call();
    }
  }

  render() {
    return (
      <View style={{ display: "flex", flexDirection: "row" }}>
        <MapView
          style={styles.map}
          ref={ref => (this.map = ref)}
          region={this.state.region}
          showsUserLocation={true}
          showsMyLocationButton={true}
          followsUserLocation={true}
          onRegionChangeComplete={
            region => this.setState({ region }) //login not working...that is only change the view when search is pressed and not when the dot is moving
          }
          onLongPress={this.handlePress}
        >
          {this.state.markers.map(marker => (
            <View key={marker.id}>
              <Marker
                draggable
                onDragStart={() => this.handleDelete(marker.id)}
                key={marker.id}
                coordinate={marker.latlng}
                title={
                  marker.title === ""
                    ? (marker.title = "Untitled Reminder")
                    : marker.title
                }
                description={
                  marker.description === ""
                    ? (marker.description =
                      " Lat: " +
                      marker.latlng.latitude.toFixed(3) +
                      " Lng: " +
                      marker.latlng.longitude.toFixed(3))
                    : marker.description +
                    " Lat: " +
                    marker.latlng.latitude.toFixed(3) +
                    " Lng: " +
                    marker.latlng.longitude.toFixed(3)
                }
              />
              <Circle
                strokeColor="#C64D45"
                fillColor="#C64D45 rgba(255, 0, 0, 0.25)"
                radius={this.state.radius}
                center={marker.latlng}
              />
            </View>
          ))}
        </MapView>

        <MarkerModal
          visible={this.state.visible}
          hide={() => this.setState({ visible: false })}
          title={this.state.title}
          onChangeTitle={title => this.setState({ title })}
          description={this.state.description}
          onChangeDescription={description => this.setState({ description })}
          saveState={this.saveState}
          selectedValue={this.state.task}
          onValueChange={(task, itemIndex) => this.setState({ task })}
          asignedTask={this.asignedTask}
        />
        <History
          history={this.props.history}
          hide={this.props.onHistoryPress}
          markers={this.state.markers}
          handleDelete={this.handleDelete}
        />
        <Settings
          settings={this.props.settings}
          hide={this.props.onSettingsPress}
          sound={this.props.sound}
          toggleSound={this.props.toggleSound}
          vibrate={this.props.vibrate}
          toggleVibration={this.props.toggleVibration}
          onValueChange={(radius, itemIndex) => this.setState({ radius })}
          selectedValue={this.state.radius}
        />
      </View>
    );
  }

  compare = () => {
    if (this.state.markers.length > 0) {
      this.state.markers.map(marker => {
        if (
          this.state.radius >=
          geolib.getDistance(marker.latlng, this.state.latlng)
        ) {
          // TODO call alarm here and pass all needed info to it
          //before u copy, check if it's the same
          //remember initially it is undefined
          if (this.state.marker.id !== marker.id) {
            this.setState({ alarm: true, marker: marker }, this.call);
          } else {
            //something should be here
          }
        } else {
          this.setState({ alarm: false });
        }
      });
    } else console.log("no markers yet");
  };

  call = appstate => {
    if (this.state.alarm === true) {
      PushNotification.localNotification({
        id: this.state.marker.id,
        ticker: "My Notification Ticker",
        autoCancel: true,
        largeIcon: "ic_launcher",
        smallIcon: "ic_notification",
        vibrate: this.props.vibrate,
        vibration: 1000,
        ongoing: false,
        priority: "high",
        visibility: "private",
        importance: "urgent",
        title: this.state.marker.title,
        message: this.state.marker.description,
        playSound: this.props.sound,
        soundName: "default",
        number: "2"
      });
      if (this.state.marker.details.task === "call") {
        Alert.alert(
          "Call Alert",
          "Call " + this.state.marker.details.phone,
          [
            {
              text: "Call",
              onPress: () =>
                Communications.phonecall(this.state.marker.details.phone, true)
            },
            {
              text: "Cancel",
              onPress: () =>
                PushNotification.cancelLocalNotifications({
                  id: this.state.marker.id
                })
            }
          ],
          { cancelable: false }
        );
      } else if (this.state.marker.details.task === "sms") {
        Alert.alert(
          "Text Alert",
          "Send text to " + this.state.marker.details.phone,
          [
            {
              text: "Send",
              onPress: () =>
                Communications.text(
                  this.state.marker.details.phone,
                  this.state.marker.details.message
                )
            },
            {
              text: "Cancel",
              onPress: () =>
                PushNotification.cancelLocalNotifications({
                  id: this.state.marker.id
                })
            }
          ],
          { cancelable: false }
        );
      } else if (this.state.marker.details.task === "email") {
        Alert.alert(
          "Email Alert",
          "Send an email to " + this.state.marker.details.email,
          [
            {
              text: "Send",
              onPress: () =>
                Communications.email(
                  [this.state.marker.email],
                  null,
                  null,
                  "Greetings",
                  this.state.marker.message
                )
            },
            {
              text: "Cancel",
              onPress: () =>
                PushNotification.cancelLocalNotifications({
                  id: this.state.marker.id
                })
            }
          ],
          { cancelable: false }
        );
      } else {
        Alert.alert(
          "Notification Alert",
          "This alert is set up to remind you that you have reached your destination",
          [
            {
              text: "Okay",
              onPress: () =>
                PushNotification.cancelLocalNotifications({
                  id: this.state.marker.id
                })
            }
          ],
          { cancelable: false }
        );
      }

      setTimeout(() => {
        this.setState(
          { alarm: false },
          PushNotification.cancelLocalNotifications({
            id: this.state.marker.id
          })
        );
      }, 1000);
    }
  };

  handlePress = e => {
    this.setState({ visible: true, coordinate: e.nativeEvent.coordinate });
  };

  checkEvent = () => {
    if (this.state.marker === "call") {
      let data = [this.state.marker.details.phone];
    }
  };

  handleSearch = () => {
    this.setState({ visible: true });
  };

  handleDelete = id => {
    const markers = this.state.markers.filter(c => c.id !== id);
    this.setState({ markers: markers });
  };

  saveState = () => {
    this.setState(
      {
        markers: [
          ...this.state.markers,
          {
            id: Date.now(),
            title: this.state.title,
            description: this.state.description,
            latlng: this.state.coordinate,
            details: {
              email: this.state.email,
              phone: this.state.phone,
              message: this.state.message,
              task: this.state.task
            }
          }
        ]
      },
      this.setState({
        visible: false,
        title: "",
        description: "",
        email: "",
        phone: "",
        message: "",
        task: ""
      })
    );
  };

  asignedTask = () => {
    if (this.state.task === "call") {
      return (
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ marginTop: 13, flex: 1, marginLeft: 5 }}>
            Phone Number:
          </Text>
          <TextInput
            style={{ flex: 2 }}
            placeholder="Enter number"
            onChangeText={phone => this.setState({ phone })}
            keyboardType={"numeric"}
            maxLength={10}
          />
        </View>
      );
    } else if (this.state.task === "sms") {
      return (
        <View>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ marginTop: 13, flex: 1, marginLeft: 5 }}>
              Phone Number:
            </Text>
            <TextInput
              style={{ flex: 2 }}
              placeholder="Enter number"
              onChangeText={phone => this.setState({ phone })}
              keyboardType={"numeric"}
              maxLength={10}
            />
          </View>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ marginTop: 13, flex: 1, marginLeft: 5 }}>
              Message
            </Text>
            <TextInput
              style={{
                flex: 2,
                height: 60,
                padding: 5,
                margin: 5,
                borderColor: "grey",
                borderWidth: 1,
                borderBottomWidth: 2,
                borderLeftWidth: 2
              }}
              placeholder="Enter text message"
              onChangeText={message => this.setState({ message })}
              multiline={true}
            />
          </View>
        </View>
      );
    } else if (this.state.task === "email") {
      return (
        <View>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ marginTop: 13, flex: 1, marginLeft: 5 }}>
              Email address:
            </Text>
            <TextInput
              style={{ flex: 2 }}
              placeholder="Enter email"
              onChangeText={email => this.setState({ email })}
              keyboardType="email-address"
            />
          </View>
          <View style={{ display: "flex", flexDirection: "row" }}>
            <Text style={{ marginTop: 13, flex: 1, marginLeft: 5 }}>
              Message
            </Text>
            <TextInput
              style={{
                flex: 2,
                height: 60,
                padding: 5,
                margin: 5,
                borderColor: "grey",
                borderWidth: 1,
                borderBottomWidth: 2,
                borderLeftWidth: 2
              }}
              placeholder="Enter email message"
              onChangeText={message => this.setState({ message })}
            />
          </View>
        </View>
      );
    } else {
      return (
        <View>
          <Text style={{ marginLeft: 20 }}>No Selected Task</Text>
        </View>
      );
    }
  };

  async findLocation() {
    try {
      const res = await Geocoder.geocodeAddress(this.props.searchPlace);
      if (res.length !== 0) {
        if (res[0].position !== null) {
          this.setState(
            {
              coordinate: {
                latitude: res[0].position.lat,
                longitude: res[0].position.lng
              },
              region: {
                latitude: res[0].position.lat,
                longitude: res[0].position.lng,
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
              }
            },
            this.map.animateToRegion(this.state.region, 500)
          );
        }
      } else {
        Alert.alert(
          "No results found",
          "The location " + this.props.searchPlace + " was not found",
          [{ text: "Okay" }, { cancelable: false }]
        );
      }
    } catch (err) {
      Alert.alert(
        "Error",
        "An error occurred while searching for the location '" +
        this.props.searchPlace +
        "'.\nPossible course: No internet access",
        [{ text: "Okay" }, { cancelable: false }]
      );
    }
  }
}

const styles = StyleSheet.create({
  map: {
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height
  },
  textStyle: {
    alignSelf: "center",
    color: "#007aff",
    fontWeight: "600",
    paddingTop: 10,
    paddingBottom: 10
  },

  buttonStyle: {
    flex: 1,
    alignSelf: "stretch",
    backgroundColor: "#fff",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "#007aff",
    marginLeft: 5,
    marginRight: 5
  }
});
