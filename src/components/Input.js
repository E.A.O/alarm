import React from "react";
import { View, Text, TextInput } from "react-native";

const Input = ({ value, onChangeText }) => {
  const { inputStyle, containerStyle } = styles;

  return (
    <View style={containerStyle}>
      <TextInput
        style={inputStyle}
        value={value}
        placeholder={"Search location"}
        onChangeText={onChangeText}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: "#000",
    backgroundColor: "white",
    paddingRight: 5,
    paddingLeft: 5,
    fontSize: 18
  },
  containerStyle: {
    height: 40,
    flex: 2,
    flexDirection: "row",
    alignItems: "center",
    paddingRight: 5
  }
};

export default Input;
