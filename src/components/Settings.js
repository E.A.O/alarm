import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  Dimensions,
  TouchableOpacity,
  Alert,
  Picker
} from "react-native";
import { CheckBox } from "react-native-elements";

export default class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      itemValue: this.props.radius,
      sound: this.props.sound,
      vibrate: this.props.vibrate
    };
  }

  render() {
    const { container, topic, section } = styles;
    return (
      <View style={container}>
        <View style={section}>
          <Image
            source={require("./icons/alarm.png")}
            style={{ height: 50, width: 50, margin: 10 }}
          />
          <Text style={topic}>Alarm Settings</Text>
        </View>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>Sound</Text>
          <CheckBox
            center
            containerStyle={{ backgroundColor: "#fff" }}
            checked={this.props.sound}
            onPress={this.props.toggleSound}
          />
        </View>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>Vibration</Text>
          <CheckBox
            center
            containerStyle={{ backgroundColor: "#fff" }}
            checked={this.props.vibrate}
            onPress={this.props.toggleVibration}
          />
        </View>
        <TouchableOpacity style={{ display: "flex", flexDirection: "column" }}>
          <Text style={{ fontSize: 18, paddingTop: 15, paddingLeft: 15 }}>
            Alarm Ringtone
          </Text>
          <Text
            style={{
              fontSize: 16,
              paddingLeft: 15,
              paddingBottom: 20
            }}
          >
            Select ringtone
          </Text>
        </TouchableOpacity>
        <View style={section}>
          <Image
            source={require("./icons/gps.png")}
            style={{ height: 50, width: 50, margin: 10 }}
          />
          <Text style={topic}>GPS Settings</Text>
        </View>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>
            GPS Alarm Range
          </Text>
          <Picker
            selectedValue={this.state.itemValue}
            style={{ height: 50, width: 110 }}
            onValueChange={(itemValue, itemIndex) =>
              this.setState({ itemValue }, this.props.changeRadius(itemValue))
            }
          >
            <Picker.Item label="200m" value={200} />
            <Picker.Item label="500m" value={500} />
            <Picker.Item label="1km" value={1000} />
            <Picker.Item label="3km" value={3000} />
            <Picker.Item label="5km" value={5000} />
          </Picker>
        </View>
        <TouchableOpacity
          onPress={() =>
            Alert.alert(
              "Contact",
              "Direct the user to a webpage or to his email which u will soon implement",
              [{ text: "Okay" }, { cancelable: false }]
            )
          }
        >
          <Text style={{ fontSize: 18, padding: 15 }}>About</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = {
  container: {
    // backgroundColor: "#8b827d",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height
  },
  topic: {
    fontSize: 20,
    fontWeight: "bold",
    margin: 20
  },
  section: {
    display: "flex",
    flexDirection: "row",
    borderWidth: 1,
    borderColor: "#fff",
    borderBottomWidth: 0,
    elevation: 1
  }
};
