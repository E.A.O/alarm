import React from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  ScrollView,
  Image,
  Alert
} from "react-native";

const History = ({ history, hide, markers, handleDelete }) => {
  let data = [1, 2, 3, 4];

  return (
    <Modal
      animationType="fade"
      transparent={false}
      visible={history}
      onRequestClose={() => hide()}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff"
        }}
      >
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            padding: 10,
            backgroundColor: "#ecebe7"
          }}
        >
          <TouchableOpacity onPress={hide}>
            <Image
              source={require("../icons/back.png")}
              style={{ height: 30, width: 30, marginRight: 30 }}
            />
          </TouchableOpacity>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>History</Text>
        </View>
        <ScrollView>
          {markers.map((marker, index) => (
            <View
              key={marker.id}
              style={{
                display: "flex",
                flexDirection: "row",
                borderBottomWidth: 1,
                borderColor: "#ddd",
                padding: 5
              }}
            >
              <Image
                source={require("../icons/gps.png")}
                style={{ height: 20, width: 20, marginTop: 10 }}
              />
              <View
                style={{ flex: 2, display: "flex", flexDirection: "column" }}
              >
                <Text numberOfLines={1} style={{ fontSize: 20 }}>
                  {marker.title}
                </Text>
                <Text numberOfLines={1} style={{ fontSize: 16 }}>
                  {marker.description}
                </Text>
              </View>
              <TouchableOpacity onPress={() => handleDelete(marker.id)}>
                <Image
                  source={require("../icons/delete.png")}
                  style={{
                    height: 20,
                    width: 20,
                    marginRight: 5,
                    marginTop: 5
                  }}
                />
              </TouchableOpacity>
            </View>
          ))}
        </ScrollView>
      </View>
    </Modal>
  );
};
export default History;
