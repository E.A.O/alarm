import React from "react";
import {
  View,
  Text,
  Modal,
  Dimensions,
  TextInput,
  Picker,
  TouchableOpacity
} from "react-native";

const AlertModal = ({
  visible,
  selectedValue,
  onValueChange,
  hide,
  title,
  description,
  saveState,
  asignedTask,
  onChangeTitle,
  onChangeDescription
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={() => {
        Alert.alert("Modal has been closed.");
      }}
    >
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <View
          style={{
            width: Dimensions.get("window").width / 1.1,
            backgroundColor: "#fff"
          }}
        >
          <View
            style={{
              backgroundColor: "#ecebe7",
              borderBottomWidth: 1,
              borderBottomColor: "#4f5858"
            }}
          >
            <Text
              style={{
                fontSize: 18,
                fontWeight: "bold",
                textAlign: "center",
                height: 40,
                paddingTop: 5
              }}
              onPress={hide}
            >
              Set Location Alarm
            </Text>
          </View>
          <View>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 18,
                  marginTop: 6,
                  flex: 1,
                  marginLeft: 5
                }}
              >
                Title
              </Text>
              <TextInput
                placeholder="Alarm title"
                style={{
                  backgroundColor: "#fff",
                  height: 40,
                  flex: 2,
                  marginRight: 5
                }}
                value={title}
                onChangeText={onChangeTitle}
              />
            </View>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 18,
                  marginTop: 7,
                  flex: 1,
                  marginLeft: 5
                }}
              >
                Desc
              </Text>
              <TextInput
                placeholder="Alarm Details"
                style={{
                  backgroundColor: "#fff",
                  height: 40,
                  flex: 2,
                  marginTop: 5,
                  marginRight: 5
                }}
                value={description}
                onChangeText={onChangeDescription}
              />
            </View>
            <View style={{ display: "flex", flexDirection: "row" }}>
              <Text
                style={{
                  fontSize: 18,
                  marginTop: 20,
                  flex: 1,
                  marginLeft: 5
                  // paddingTop: 4
                }}
              >
                Task:
              </Text>

              <Picker
                selectedValue={selectedValue}
                style={{ height: 50, width: 110 }}
                onValueChange={onValueChange}
              >
                <Picker.Item label="None" value="nothing" />
                <Picker.Item label="Call" value="call" />
                <Picker.Item label="SMS" value="sms" />
                <Picker.Item label="Email" value="email" />
              </Picker>
            </View>
            {asignedTask()}
            <View
              style={{
                display: "flex",
                flexDirection: "row",
                margin: 10
              }}
            >
              <Text style={{ flex: 1 }} />
              <TouchableOpacity
                onPress={saveState}
                style={{
                  flex: 1,
                  backgroundColor: "#e8e8e8",
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: "#ecebe7",
                  marginLeft: 2,
                  marginRight: 2
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    fontWeight: "600",
                    paddingTop: 5,
                    paddingBottom: 5
                  }}
                >
                  Save
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={hide}
                style={{
                  flex: 1,
                  backgroundColor: "#e8e8e8",
                  borderRadius: 5,
                  borderWidth: 1,
                  borderColor: "#ecebe7",
                  marginLeft: 2,
                  marginRight: 2
                }}
              >
                <Text
                  style={{
                    alignSelf: "center",
                    fontWeight: "600",
                    paddingTop: 5,
                    paddingBottom: 5
                  }}
                >
                  Cancel
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
};
export default AlertModal;
