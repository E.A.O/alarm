import React from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Image,
  Picker,
  Alert
} from "react-native";
import { CheckBox } from "react-native-elements";

const Settings = ({
  settings,
  hide,
  sound,
  toggleSound,
  vibrate,
  toggleVibration,
  selectedValue,
  onValueChange
}) => {
  return (
    <Modal
      animationType="fade"
      transparent={false}
      visible={settings}
      onRequestClose={() => hide()}
    >
      <View
        style={{
          flex: 1,
          backgroundColor: "#fff"
        }}
      >
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            padding: 10,
            backgroundColor: "#ecebe7"
          }}
        >
          <TouchableOpacity onPress={hide}>
            <Image
              source={require("../icons/back.png")}
              style={{ height: 30, width: 30, marginRight: 30 }}
            />
          </TouchableOpacity>
          <Text style={{ fontSize: 20, fontWeight: "bold" }}>Settings</Text>
        </View>
        <View
          style={{
            display: "flex",
            flexDirection: "row",
            borderWidth: 1,
            borderColor: "#fff",
            borderBottomWidth: 0,
            elevation: 1
          }}
        >
          <Image
            source={require("../icons/alarm.png")}
            style={{ height: 40, width: 40, marginRight: 10 }}
          />
          <Text style={{ fontSize: 20, fontWeight: "bold", margin: 10 }}>
            Alarm Settings
          </Text>
        </View>

        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>Sound</Text>
          <CheckBox
            center
            containerStyle={{ backgroundColor: "#fff" }}
            checked={sound}
            onPress={toggleSound}
          />
        </View>
        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>Vibration</Text>
          <CheckBox
            center
            containerStyle={{ backgroundColor: "#fff" }}
            checked={vibrate}
            onPress={toggleVibration}
          />
        </View>

        <TouchableOpacity style={{ display: "flex", flexDirection: "column" }}>
          <Text style={{ fontSize: 18, paddingTop: 15, paddingLeft: 15 }}>
            Alarm Ringtone
          </Text>
          <Text
            style={{
              fontSize: 16,
              paddingLeft: 15,
              paddingBottom: 20
            }}
          >
            Select ringtone
          </Text>
        </TouchableOpacity>

        <View
          style={{
            display: "flex",
            flexDirection: "row",
            borderTopWidth: 2,
            borderTopColor: "#ddd",
            borderColor: "#fff",
            borderBottomWidth: 0
          }}
        >
          <Image
            source={require("../icons/alarm.png")}
            style={{ height: 40, width: 40, marginRight: 10 }}
          />
          <Text style={{ fontSize: 20, fontWeight: "bold", margin: 10 }}>
            GPS Settings
          </Text>
        </View>

        <View style={{ display: "flex", flexDirection: "row" }}>
          <Text style={{ fontSize: 18, flex: 2, padding: 15 }}>
            GPS Alarm Range
          </Text>
          <Picker
            selectedValue={selectedValue}
            style={{ height: 50, width: 110 }}
            onValueChange={onValueChange}
          >
            <Picker.Item label="200m" value={200} />
            <Picker.Item label="500m" value={500} />
            <Picker.Item label="1km" value={1000} />
            <Picker.Item label="3km" value={3000} />
            <Picker.Item label="5km" value={5000} />
          </Picker>
        </View>
        <TouchableOpacity
          onPress={() =>
            Alert.alert(
              "Contact E.A.O Designs",
              "Direct the user to a webpage or to his email which u will soon implement",
              [{ text: "Okay" }, { cancelable: false }]
            )
          }
        >
          <Text style={{ fontSize: 18, padding: 15 }}>About</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};
export default Settings;
