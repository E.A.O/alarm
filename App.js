import React, { Component } from "react";
import { View, Dimensions } from "react-native";
import Header from "./src/components/Header";
import ShowMap from "./src/components/ShowMap";
import { PermissionsAndroid } from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.requestCameraPermission();
    this.state = {
      sound: true,
      vibrate: true,
      search: false,
      searchPlace: "",
      value: "",
      settings: false,
      history: false,
      screenWidth: Dimensions.get("window").width,
      screenHeight: Dimensions.get("window").height
    };
  }

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: "Location app wants use GPS services",
          message:
            "Location app will need to turn GPS on for effective work done"
        }
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log("You can use the GPS");
      } else {
        console.log("GPS permission denied");
      }
    } catch (err) {
      console.warn(err);
    }
  }

  render() {
    return (
      <View>
        <Header
          onChangeText={searchPlace => this.setState({ searchPlace })}
          searchPlace={this.state.searchPlace}
          onSettingsPress={() =>
            this.setState({
              settings: !this.state.settings
            })
          }
          onHistoryPress={() =>
            this.setState({
              history: !this.state.history
            })
          }
          onSearchPress={() =>
            this.setState(
              {
                search: true
              },
              () =>
                setTimeout(() => {
                  this.setState({ search: false });
                }, 1000)
            )
          }
        />
        <View
          style={{
            display: "flex",
            width: Dimensions.get("window").width,
            flexDirection: "row"
          }}
        >
          <ShowMap
            settings={this.state.settings}
            history={this.state.history}
            search={this.state.search}
            sound={this.state.sound}
            toggleSound={() =>
              this.setState({
                sound: !this.state.sound
              })
            }
            vibrate={this.state.vibrate}
            toggleVibration={() =>
              this.setState({
                vibrate: !this.state.vibrate
              })
            }
            searchPlace={this.state.searchPlace}
            onHistoryPress={() =>
              this.setState({
                history: !this.state.history
              })
            }
            onSettingsPress={() =>
              this.setState({
                settings: !this.state.settings
              })
            }
          />
        </View>
      </View>
    );
  }
}
